// 1. Це функція, яка використовує своє власне викликання для розв'язання завдання.

// Функція для підрахунку факторіалу числа
function factorial(number) {
    if (number === 0 || number === 1) {
        return 1;
    } else {
        return number * factorial(number - 1);
    }
}

// Функція для перевірки коректності введення числа
function validateInput(input) {
    const number = Number(input);
    if (Number.isNaN(number)) {
        alert('Введіть коректне число!');
        return false;
    }
    return true;
}

// Отримання введеного користувачем числа та розрахунок факторіалу
const button = document.getElementById('calculate');
button.addEventListener('click', () => {
    let input = document.getElementById('number').value;
    if (!validateInput(input)) {
        return;
    }
    const number = Number(input);
    const result = factorial(number);
    document.getElementById('result').innerHTML = `Факторіал числа ${number} дорівнює ${result}`;
});